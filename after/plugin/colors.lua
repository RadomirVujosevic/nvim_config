require("catppuccin").setup({
    flavour = "macchiato",
    background = {
        light = "latte",
        dark = "mocha"
    },
    
})

vim.o.termguicolors = true
vim.cmd.colorscheme "catppuccin"
