vim.g.mapleader = " "
vim.keymap.set("n", "<leader>pv", vim.cmd.Ex)

vim.keymap.set("n", "<C-s>", "<cmd> w<CR>")
vim.keymap.set("i", "<C-s>", "<Esc> <cmd> w<CR>a")
vim.keymap.set("n", "<C-h>", "<cmd> TmuxNavigateLeft<CR>" )
vim.keymap.set("n", "<C-l>", "<cmd> TmuxNavigateRight<CR>" )
vim.keymap.set("n", "<C-j>", "<cmd> TmuxNavigateDown<CR>" )
vim.keymap.set("n", "<C-k>", "<cmd> TmuxNavigateUp<CR>")
